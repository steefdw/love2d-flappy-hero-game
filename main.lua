love.window.setTitle('flappy hero')
love.window.maximize()
love.window.setFullscreen(false)

world = {
    width = love.graphics.getWidth(),
    height = love.graphics.getHeight(),
    score = 0,
    upcomingPipe = 1,
    color = {.14, .36, .46},
    draw = function(self)
        love.graphics.setColor(self.color[1], self.color[2], self.color[3])
        love.graphics.rectangle('fill', 0, 0, self.width, self.height)
    end
}

bird = {
    x = world.width/4.9,
    y = 0,
    ySpeed = 0,
    width = world.width/10,
    height = world.height/15.52,
    img = nil,
    draw = function(self)
        love.graphics.setColor(.90, .90, .90)
        local sx = self.width / self.img:getWidth()
        local sy = self.height / self.img:getHeight()
        love.graphics.draw(self.img, self.x, self.y, 0, sx, sy, 0, 0)
    end
}

pipe = {
    spaceHeight = world.height/3.88,
    width = world.width/4.9,
    x = 0,
    spaceY = 0,
    speed = world.width/5,
    newY = function()
        local spaceYMin = pipe.width
        local spaceYMax = world.height - pipe.spaceHeight - spaceYMin

        return love.math.random(spaceYMin, spaceYMax)
    end,
    new = function(self)
        local object =  {}
        self.__index  = self
        setmetatable(object, self)

        object.x = world.width
        object.spaceY = pipe.newY()

        return object
    end,
    movePipe = function(self, dt)
        self.x = self.x - (self.speed * dt)

        if (self.x + self.width) < 0 then
            self.x = world.width
            self.spaceY = self.newY()
        end

        return self
    end,
    isCollidingWithBird = function(self)
        return
        -- Left edge of bird is to the left of the right edge of pipe
        bird.x < (self.x + self.width)
        -- Right edge of bird is to the right of the left edge of pipe
        and (bird.x + bird.width) > self.x
        and (
            -- Top edge of bird is above the bottom edge of first pipe segment
            bird.y < self.spaceY
            or
            -- Bottom edge of bird is below the top edge of second pipe segment
            (bird.y + bird.height) > (self.spaceY + self.spaceHeight)
        )
    end,
    color = {},
    draw = function(self)
        love.graphics.setColor(self.color[1], self.color[2], self.color[3])
        love.graphics.rectangle('fill',
            self.x,
            0,
            self.width,
            self.spaceY)
        love.graphics.rectangle('fill',
            self.x,
            self.spaceY + self.spaceHeight,
            self.width,
            world.height - self.spaceY - self.spaceHeight)
    end
}

pipes = {}

function world:reset()
    bird.y = world.height/1.94
    bird.ySpeed = 0
    bird.img = love.graphics.newImage('gfx/alien1.png')

    pipe.color = {.37, .82, .28}
    pipes[1] = pipe:new()
    pipes[2] = pipe:new()
    pipes[2].x = world.width + ((world.width + pipe.width) / 2)

    world.score = 0
    world.upcomingPipe = 1
    world.color = {.14, .36, .46}
end

function love.load()
    world.reset()
end

function love.update(dt)
    bird.ySpeed = bird.ySpeed + (516 * dt)
    bird.y = bird.y + (bird.ySpeed * dt)

    pipes[1]:movePipe(dt)
    pipes[2]:movePipe(dt)

    if pipes[1]:isCollidingWithBird()
        or pipes[2]:isCollidingWithBird()
        or bird.y > world.height
    then
        world.reset()
    end

    local function updateScoreAndClosestPipe(thisPipe, currentPipe, otherPipe)
        if world.upcomingPipe == thisPipe
            and (bird.x > (currentPipe.x + pipe.width))
        then
            world.score = world.score + 1
            world.upcomingPipe = otherPipe
            world.color[1] = world.color[1]+0.04
            world.color[2] = world.color[2]-0.02
            world.color[3] = world.color[3]-0.02
            pipes[1].speed = pipes[1].speed + world.width/100
            pipes[2].speed = pipes[2].speed + world.width/100

            -- change the hero + pipes after receiving enough points
            if(world.score == 5) then
                pipe.color = {.92, 1, 0} -- yellow pipes
                bird.img = love.graphics.newImage('gfx/hero-flash-100.png')
            elseif(world.score == 10) then
                pipe.color = {0, 0, 0}   -- black pipes
                bird.img = love.graphics.newImage('gfx/hero-batgirl-100.png')
            end
        end
    end

    updateScoreAndClosestPipe(1, pipes[1], 2)
    updateScoreAndClosestPipe(2, pipes[2], 1)
end

function love.draw()
    world:draw()
    bird:draw()

    pipes[1]:draw()
    pipes[2]:draw()

    love.graphics.setColor(1, 1, 1)
    love.graphics.print('Score: '..world.score, 15, 15)

    love.graphics.setColor(1, 1, 1)
    love.graphics.print('Speed: '..pipes[1].speed, 15, 35)
end

function love.keypressed()
    if bird.y > 0 then
        local factor = 4
        if(bird.ySpeed < -400) then
            factor = 7
        elseif(bird.ySpeed < -200) then
            factor = 6
        elseif(bird.ySpeed < -100) then
            factor = 5
        end
        bird.ySpeed = -(bird.height*factor)
    end
end

